package com.mycompany.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class Main
{
    public static void main( String[] args ) throws IOException {

        String path = args[0];
        String language = args[1].toLowerCase();

        validateInput(path, language);

        IndexBuilder indexBuilder = new IndexBuilder();
        indexBuilder.createIndexes(path, language);

    }

    private static void validateInput(String path, String language){
        if (language.length() != 2){
            System.out.println("Language option must follow the lowercase ISO 639-1 standard, such as 'en'");
            return;
        }
        Path enteredPath = Paths.get(path);
        if (Files.notExists(enteredPath)) {
            System.out.println("Faulty path provided as argument. Path does not exist.");
            return;
        }
    }
}
