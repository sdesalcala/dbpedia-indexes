package com.mycompany.app;

import net.openhft.chronicle.map.ChronicleMap;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class IndexBuilder {

    private static ChronicleMap<String, String> formToMeaningIndex;
    // we need this to create index1's redirect part
    // this will be an URI to label index
    private static ChronicleMap<String, String> meaningToFormIndex;

    private static ChronicleMap<String, String[]> meaningToGlosses;

    private static ChronicleMap<String, String[]> formToLexicalizations;

    private static String path;
    private static String language;

    public void createIndexes(String path, String language) throws IOException {
        this.path = path;
        this.language = language;
        createFormToMeaningIndex(); // it's necessary to create index one in order to create index two
        createMeaningToGlosses();
        closeIndexes();
    }

    private void closeIndexes(){
        formToMeaningIndex.close();
        meaningToFormIndex.close();
        meaningToGlosses.close();
        formToLexicalizations.close();
    }

    //format label => URI (including all redirects of each article)
    private static void createFormToMeaningIndex() throws IOException {

        createFormToMeaningIndexFile();
        createMeaningToFormIndexFile();
        createFormToLexicalizationsFile();
        // we first use labels_lang\=en to map the label to URI
        mapLabelsToURIs();
        // now we use redirects_lang\=en_transitive to map each redirect
        // since this file uses the URI instead of the label, we use the previously created index to get the label
        // this way, we can map (REDIRECT_LABEL => REDIRECTED_URI)
        addRedirectsToFormToMeaningIndex();
    }

    public static void createFormToMeaningIndexFile() throws IOException {
        String indexOnePath = Paths.get(path, "formToMeaningIndex_" + language + ".bin").toString();
        File indexOneFile = new File(indexOnePath);

        formToMeaningIndex = ChronicleMap
                .of(String.class, String.class)
                .name("formToMeaningIndex_" + language)
                .entries(27191196)
                .averageValue("<http://dbpedia.org/resource/Robert_Koch>")
                .averageKey("Battle of Ardrianople")
                .createOrRecoverPersistedTo(indexOneFile, true);

    }

    public static void createMeaningToFormIndexFile() throws IOException {
        String indexOneHelperPath = Paths.get(path, "meaningToFormIndex_" + language + ".bin").toString();
        File indexOneHelperFile = new File(indexOneHelperPath);

        meaningToFormIndex = ChronicleMap
                .of(String.class, String.class)
                .name("meaningToFormIndex_" + language)
                .entries(27191196)
                .averageValue("Battle of Ardrianople")
                .averageKey("<http://dbpedia.org/resource/Robert_Koch>")
                .createOrRecoverPersistedTo(indexOneHelperFile, true);
    }

    private static void createMeaningToGlossesFile() throws IOException {
        String indexTwoPath = Paths.get(path, "meaningToGlosses_" + language + ".bin").toString();

        File indexTwoFile = new File(indexTwoPath);

        meaningToGlosses = ChronicleMap
                .of(String.class, String[].class)
                .name("meaningToGlosses_" + language)
                .entries(6000000)
                .averageValue(new String[]{"!Action pact!","!!Destroy-Oh-Boy!! is the debut album by the American garage punk group New Bomb Turks. It was released in 1993 by Crypt Records. It was released to very positive reviews."})
                .averageKey("<http://dbpedia.org/resource/Robert_Koch>")
                .createOrRecoverPersistedTo(indexTwoFile, true);
    }

    private static void createFormToLexicalizationsFile() throws IOException {

        String formToLexicalizationsPath = Paths.get(path, "formToLexicalizations_" + language + ".bin").toString();

        File formToLexicalizationsFile = new File(formToLexicalizationsPath);

        formToLexicalizations = ChronicleMap
                .of(String.class, String[].class)
                .name("formToLexicalizations_" + language)
                .entries(6000000)
                .averageValue(new String[]{"!Action pact!","!!Destroy-Oh-Boy!! is the debut album by the American garage punk group New Bomb Turks. It was released in 1993 by Crypt Records. It was released to very positive reviews."})
                .averageKey("<http://dbpedia.org/resource/Robert_Koch>")
                .createOrRecoverPersistedTo(formToLexicalizationsFile, true);

    }

    private static void mapLabelsToURIs(){
        String labelsFile = Paths.get(path, "labels_lang=" + language + ".ttl").toString();

        try (Stream<String> lines = Files.lines(Paths.get(labelsFile), Charset.defaultCharset())) {
            lines.forEachOrdered(line -> storeLabelURIPair(line));
        } catch (IOException e) {
            System.out.println("File not found. Did you set the language correctly?");
            e.printStackTrace();
        }

    }

    private static void storeLabelURIPair(String line){

        String[] pair = divideString(line);
        formToMeaningIndex.put(pair[0], pair[1]);
        meaningToFormIndex.put(pair[1], pair[0]);

    }

    private static String[] divideString(String line){
        String URI = AppUtils.getURIFromLabelsFile(line);
        String label = AppUtils.returnInQuotes(line);
        return new String[]{label, URI};
    }

    private static void addRedirectsToFormToMeaningIndex(){

        mapRedirectsToURIs();

    }

    private static void mapRedirectsToURIs(){

        String redirectsFile = Paths.get(path, "redirects_lang=" + language + "_transitive.ttl").toString();

        try (Stream<String> lines = Files.lines(Paths.get(redirectsFile), Charset.defaultCharset())) {
            lines.forEachOrdered(line -> storeRedirectURIPair(line));
        } catch (IOException e) {
            System.out.println("File not found. Did you set the language correctly?");
            e.printStackTrace();
        }

    }

    private static void storeRedirectURIPair(String line){

        String[] returned = AppUtils.divideRedirectString(line);
        // uri => uri it redirects to
        // http://dbpedia.org/resource/AfghanistanMilitary http://dbpedia.org/resource/Afghan_Armed_Forces
        String label = meaningToFormIndex.get(returned[0]);
        // there are some articles on the redirects file that are not present in the labels file and don't have a label
        // all of them seem to start with "http://dbpedia.org/resource/Template:" (or File instead of Template)
        if (label == null){
            System.out.println(returned[0] + " returned nothing ");
        } else {
            formToMeaningIndex.put(label, returned[1]);
            addToLexicalizationsList(returned);
        }
    }

    private static void addToLexicalizationsList(String[] pair){
        String[] lexicalizations = formToLexicalizations.get(pair[1]);
        if (lexicalizations == null){
            formToLexicalizations.put(pair[1], new String[]{pair[0]});
        } else {
            formToLexicalizations.put(pair[1], addX(lexicalizations, pair[0]));
        }
    }

    private static String[] addX(String[] arr, String s) {
        String[] newArray = new String[arr.length + 1];
        for (int i = 0; i < arr.length; i++)
            newArray[i] = arr[i];
        newArray[arr.length] = s;
        return newArray;
    }

    private static void createMeaningToGlosses() throws IOException {

        createMeaningToGlossesFile();

        mapURIsToLabelAbstractPair();

    }


    private static void mapURIsToLabelAbstractPair(){
        String shortAbstractsFile = Paths.get(path, "short-abstracts_lang=" + language + ".ttl").toString();

        try (Stream<String> lines = Files.lines(Paths.get(shortAbstractsFile), Charset.defaultCharset())) {
            lines.forEachOrdered(line -> storeURIsToLabelAbstractPair(line));
        } catch (IOException e) {
            System.out.println("File not found. Did you set the language correctly?");
            e.printStackTrace();
        }

    }

    private static void storeURIsToLabelAbstractPair(String line){
        String[] elements = divideURIsToLabelAbstractPairLine(line);

        if (elements[1] != null){
            System.out.println(elements[0] + " : " + elements[1] + " , " + elements[2]);
            meaningToGlosses.put(elements[0], new String[]{elements[1], elements[2]});
        } else {
            System.out.println("No article found for : " + elements[0] + " , " + elements[2]);
        }
    }

    private static String[] divideURIsToLabelAbstractPairLine(String line){
        String shortAbstract = AppUtils.returnInQuotes(line);
        String uri = AppUtils.returnInBrackets(line);
        String label = meaningToFormIndex.get(uri);
        return new String[]{uri, label, shortAbstract};
    }

}
