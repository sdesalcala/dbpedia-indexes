package com.mycompany.app;

// class for useful functions used throughout the program that do not fit in other classes

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtils {

    // this returns the part of the sentence that fits the pattern "blablabla"@en
    public static String returnInQuotes(String line){
        Pattern pattern = Pattern.compile("\"([^\"]*)\"@");
        Matcher matcher = pattern.matcher(line);
        matcher.find();
        return matcher.group(1);
    }

    // this returns the URI from each line in the labels file
    public static String getURIFromLabelsFile(String line){
        String[] partsOfLine = line.split("\\s+"); //split by spaces
        return partsOfLine[0].substring(1, partsOfLine[0].length() - 1); // remove first and last character
    }

    public static String[] divideRedirectString(String line){

        Pattern pattern = Pattern.compile("\\<(.*?)\\>");
        Matcher matcher = pattern.matcher(line);
        matcher.find();
        String uri = matcher.group(1);
        matcher.find();
        matcher.find();
        String redirect = matcher.group(1);

        return new String[]{uri, redirect};
    }

    public static String returnInBrackets(String line){
        Pattern pattern = Pattern.compile("\\<(.*?)\\>");
        Matcher matcher = pattern.matcher(line);
        matcher.find();
        return matcher.group(1);
    }
}
