package com.mycompany.app;

import com.ibm.icu.util.ULocale;
import net.openhft.chronicle.map.ChronicleMap;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// meaningdictionary does not create indexes, you just provide the path where the created indexes are

public class MeaningDictionaryImplementation implements MeaningDictionary {

    String path; // path where the binary files are
    String language; // language of the files. two character lowercase.

    private ChronicleMap<String, String> formToMeaningIndex; // label -> URI
    private ChronicleMap<String, String> meaningToFormIndex; // URI -> label
    private ChronicleMap<String, String[]> meaningToGlosses; // URI -> glosses (label + short description)
    private ChronicleMap<String, String[]> formToLexicalizations; // label -> redirect URIs

    public MeaningDictionaryImplementation(String path, String language){
        this.path = path;
        this.language = language;
    }

    public void loadIndexes() throws IOException {
        loadFormToMeaningIndexFile();
        loadMeaningToFormIndexFile();
        loadMeaningToGlossesIndexFile();
        loadFormToLexicalizationsIndexFile();
    }

    public void loadFormToMeaningIndexFile() throws IOException {
        String indexOnePath = Paths.get(path, "formToMeaningIndex_" + language + ".bin").toString();
        File indexOneFile = new File(indexOnePath);

        formToMeaningIndex = ChronicleMap
                .of(String.class, String.class)
                .name("formToMeaningIndex_" + language)
                .entries(27191196)
                .averageValue("<http://dbpedia.org/resource/Robert_Koch>")
                .averageKey("Battle of Ardrianople")
                .recoverPersistedTo(indexOneFile, true);
    }

    public void loadMeaningToFormIndexFile() throws IOException {
        String indexOneHelperPath = Paths.get(path, "meaningToFormIndex_" + language + ".bin").toString();
        File indexOneHelperFile = new File(indexOneHelperPath);

        meaningToFormIndex = ChronicleMap
                .of(String.class, String.class)
                .name("meaningToFormIndex_" + language)
                .entries(27191196)
                .averageValue("Battle of Ardrianople")
                .averageKey("<http://dbpedia.org/resource/Robert_Koch>")
                .recoverPersistedTo(indexOneHelperFile, true);
    }

    private void loadMeaningToGlossesIndexFile() throws IOException {
        String indexTwoPath = Paths.get(path, "meaningToGlosses_" + language + ".bin").toString();
        File indexTwoFile = new File(indexTwoPath);

        meaningToGlosses = ChronicleMap
                .of(String.class, String[].class)
                .name("meaningToGlosses_" + language)
                .entries(6000000)
                .averageValue(new String[]{"!Action pact!","!!Destroy-Oh-Boy!! is the debut album by the American garage punk group New Bomb Turks. It was released in 1993 by Crypt Records. It was released to very positive reviews."})
                .averageKey("<http://dbpedia.org/resource/Robert_Koch>")
                .recoverPersistedTo(indexTwoFile, true);
    }

    private void loadFormToLexicalizationsIndexFile() throws IOException {
        String formToLexicalizationsPath = Paths.get(path, "formToLexicalizations_" + language + ".bin").toString();
        File formToLexicalizationsFile = new File(formToLexicalizationsPath);

        formToLexicalizations = ChronicleMap
                .of(String.class, String[].class)
                .name("formToLexicalizations_" + language)
                .entries(6000000)
                .averageValue(new String[]{"!Action pact!","!!Destroy-Oh-Boy!! is the debut album by the American garage punk group New Bomb Turks. It was released in 1993 by Crypt Records. It was released to very positive reviews."})
                .averageKey("<http://dbpedia.org/resource/Robert_Koch>")
                .recoverPersistedTo(formToLexicalizationsFile, true);
    }

    // you should use this function when you're done
    public void closeIndexes(){
        formToMeaningIndex.close();
        meaningToFormIndex.close();
        meaningToGlosses.close();
        formToLexicalizations.close();
    }

    // dbpedia entries are always simple
    @Override
    public POS.Tagset getTagset() {
        return POS.Tagset.Simple;
    }

    @Override
    public Stream<String> getMeaningsStream() {
        return formToMeaningIndex.values().stream();
    }

    @Override
    public Stream<String> getMeaningsStream(ULocale language) {
        if (!language.getLanguage().equals(this.language)){
            System.out.println("Your locale language of choice " + language.getLanguage() + " does not match file language " + this.language);
            return Stream.empty();
        } else return getMeaningsStream();
    }

    @Override
    public Set<String> getMeanings(ULocale language) {
        return meaningToFormIndex.keySet();
    }

    // input : DBPedia page name in specified language
    // output: URI of that DBPedia page
    @Override
    public List<String> getMeanings(String form, ULocale language) {
        if (!language.getLanguage().equals(this.language)){
            System.out.println("Your locale language of choice " + language.getLanguage() + " does not match file language " + this.language);
            return Collections.emptyList();
        }
        String returned = formToMeaningIndex.get(form);
        if (returned == null){
            return Collections.emptyList();
        } else return Arrays.asList(new String[]{returned});
    }

    @Override
    public List<String> getMeanings(String form, POS.Tag pos, ULocale language) {
        if (pos != POS.Tag.NOUN){
            return Collections.emptyList();
        } else return getMeanings(form, language);
    }

    // input : URI
    // output : boolean that is true if the index contains such URI
    @Override
    public boolean contains(String id) {
        return (meaningToFormIndex.get(id) != null);
    }

    // input URI
    // output label
    @Override
    public Optional<String> getLabel(String id, ULocale language) {
        if (!language.getLanguage().equals(this.language)){
            System.out.println("Your locale language of choice " + language.getLanguage() + " does not match file language " + this.language);
        }
        String returned = meaningToFormIndex.get(id);
        if (returned == null){
            String redirectReturned = getRedirectURI(id, language);
            if (redirectReturned == null){
                return Optional.empty();
            } else return Optional.of(redirectReturned);
        }
        return Optional.of(returned);
    }

    // input : URI
    // output : URI of the page the input URI redirects to
    public String getRedirectURI(String id, ULocale language){
        String label = getLabel(id, language).get();
        String redirectURI = getMeanings(label, language).get(0);
        return redirectURI;
    }

    // dbpedia does not provide this info so we always return empty
    @Override
    public Optional<Boolean> isNE(String id) {
        return Optional.empty();
    }

    // input URI
    // output glosses
    @Override
    public List<String> getGlosses(String id, ULocale language) {
        if (!language.getLanguage().equals(this.language)){
            System.out.println("Your locale language of choice " + language.getLanguage() + " does not match file language " + this.language);
        }
        String[] returned = meaningToGlosses.get(id);
        if (returned == null){
            String redirectReturned = getRedirectURI(id, language);
            System.out.println(redirectReturned);
            returned = meaningToGlosses.get(redirectReturned);
            if (returned == null){
                return Collections.emptyList();
            } else return Arrays.asList(returned);
        }
        return Arrays.asList(returned);
    }

    // not applicable to dbpedia so we return nothing
    @Override
    public List<String> getHyponyms(String id, int max_depth) {
        return Collections.emptyList();
    }
    // not applicable to dbpedia so we return nothing
    @Override
    public List<String> getHypernyms(String id, int max_depth) {
        return Collections.emptyList();
    }
    // not applicable to dbpedia so we return nothing
    @Override
    public List<String> getRelatedMeanings(String id, int max_depth) {
        return Collections.emptyList();
    }

    @Override
    public Stream<Triple<String, POS.Tag, ULocale>> getLexicalizationsStream() {
        return formToLexicalizations.values().stream().flatMap(Arrays::stream)
                .map(x -> new ImmutableTriple<String, POS.Tag, ULocale>(x, POS.Tag.NOUN, ULocale.forLanguageTag(language)));
    }

    @Override
    public Stream<Pair<String, POS.Tag>> getLexicalizationsStream(ULocale language) {
        if (!language.getLanguage().equals(this.language)){
            System.out.println("Your locale language of choice " + language.getLanguage() + " does not match file language " + this.language);
        }
        return formToLexicalizations.values().stream().flatMap(Arrays::stream)
                .map(x -> new ImmutablePair<String, POS.Tag>(x, POS.Tag.NOUN));
    }

    @Override
    public Set<Pair<String, POS.Tag>> getLexicalizations(ULocale language) {
        if (!language.getLanguage().equals(this.language)){
            System.out.println("Your locale language of choice " + language.getLanguage() + " does not match file language " + this.language);
        }
        return getLexicalizationsStream(language).collect(Collectors.toSet());
    }

    // input : URI
    // output: URIs that redirect to input URI
    @Override
    public List<Pair<String, POS.Tag>> getLexicalizations(String id) {
        String[] returned = formToLexicalizations.get(id);
        if (returned == null){
            return Collections.emptyList();
        } else {
            List<Pair<String, POS.Tag>> pairList = new ArrayList<>();
            for (String lex : returned){
                pairList.add(new ImmutablePair<String, POS.Tag>(lex, POS.Tag.NOUN));
            }
            return pairList;
        }
    }

    @Override
    public List<Pair<String, POS.Tag>> getLexicalizations(String id, ULocale language) {
        if (!language.getLanguage().equals(this.language)){
            System.out.println("Your locale language of choice " + language.getLanguage() + " does not match file language " + this.language);
        } else return getLexicalizations(id);
        return null;
    }
}
