package com.mycompany.app;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import com.ibm.icu.util.ULocale;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;


public interface MeaningDictionary {

    POS.Tagset getTagset();
    Stream<String> getMeaningsStream();
    Stream<String> getMeaningsStream(ULocale language);
    Set<String> getMeanings(ULocale language);
    // List of meanings sorted according to dictionary criteria, e.g. frequency, best sense first, etc.
    List<String> getMeanings(String form, ULocale language);
    List<String> getMeanings(String form, POS.Tag pos, ULocale language);

    boolean contains(String id);
    Optional<String> getLabel(String id, ULocale language);
    Optional<Boolean> isNE(String id);
    List<String> getGlosses(String id, ULocale language);
    List<String> getHyponyms(String id, int max_depth);
    List<String> getHypernyms(String id, int max_depth);
    List<String> getRelatedMeanings(String id, int max_depth);


    Stream<Triple<String, POS.Tag, ULocale>> getLexicalizationsStream();
    Stream<Pair<String, POS.Tag>> getLexicalizationsStream(ULocale language);
    Set<Pair<String, POS.Tag>> getLexicalizations(ULocale language);
    // List of lexicalizations sorted according to dictionary criteria
    List<Pair<String, POS.Tag>> getLexicalizations(String id);
    List<Pair<String, POS.Tag>> getLexicalizations(String id, ULocale language);


}
