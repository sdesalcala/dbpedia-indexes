package com.mycompany.app;

import com.ibm.icu.util.ULocale;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.*;

import java.util.*;
import static org.junit.Assert.assertEquals;

// many of these tests assume a specific version of the english-language files were used to create the index
// they are the ones specified in the readme
// if you don't use these files, some tests may fail

public class MeaningDictionaryImplementationTest {

    String path = "/home/sandor/Desktop/"; // change as needed
    String language = "en"; // change as needed
    MeaningDictionaryImplementation meaningDictionaryImplementation;

    @Before
    public void setUp() throws Exception {
        meaningDictionaryImplementation = new MeaningDictionaryImplementation(path, language);
        meaningDictionaryImplementation.loadIndexes();
    }

    @After
    public void setDown(){
        meaningDictionaryImplementation.closeIndexes();
    }

    @Test
    public void getTagsetTest(){
        POS.Tagset expected = POS.Tagset.Simple;
        POS.Tagset returned = meaningDictionaryImplementation.getTagset();
        assertEquals(expected, returned);
    }

    @Test
    public void getMeaningWithFormAndLocaleTest(){
        ULocale usedLanguage = ULocale.forLanguageTag(language);
        String label = "Afghan Armed Forces";
        String expectedURI = "http://dbpedia.org/resource/Afghan_Armed_Forces";
        List<String> actualURI = meaningDictionaryImplementation.getMeanings(label, usedLanguage);
        assertEquals(expectedURI, actualURI.get(0));
    }

    @Test
    public void getMeaningWithFormAndLocaleRedirectTest(){
        ULocale usedLanguage = ULocale.forLanguageTag(language);
        String label = "AfghanistanMilitary";
        String expectedURI = "http://dbpedia.org/resource/Afghan_Armed_Forces";
        List<String> actualURI = meaningDictionaryImplementation.getMeanings(label, usedLanguage);
        assertEquals(expectedURI, actualURI.get(0));
    }

    @Test
    public void getMeaningsWithFormPOSAndLocaleTest(){
        ULocale usedLanguage = ULocale.forLanguageTag(language);
        String label = "AfghanistanMilitary";
        String expectedURI = "http://dbpedia.org/resource/Afghan_Armed_Forces";
        List<String> actualURI = meaningDictionaryImplementation.getMeanings(label, POS.Tag.NOUN, usedLanguage);
        assertEquals(expectedURI, actualURI.get(0));
    }

    @Test
    public void getMeaningsWithFormPOSAndLocaleNonNounTest(){
        ULocale usedLanguage = ULocale.forLanguageTag(language);
        String label = "AfghanistanMilitary";
        List<String> expected = Collections.emptyList();
        List<String> actual = meaningDictionaryImplementation.getMeanings(label, POS.Tag.ADJ, usedLanguage);
        assertEquals(expected, actual);
    }

    @Test
    public void containsTest(){
        String inputURI = "http://dbpedia.org/resource/AfghanistanMilitary";
        boolean expectedOutput = true;
        boolean actualOutput = meaningDictionaryImplementation.contains(inputURI);
        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void getLabelTest(){
        ULocale usedLanguage = ULocale.forLanguageTag(language);
        String uri = "http://dbpedia.org/resource/Afghan_Armed_Forces";
        String expectedLabel = "Afghan Armed Forces";
        Optional<String> actualLabel = meaningDictionaryImplementation.getLabel(uri, usedLanguage);
        assertEquals(expectedLabel, actualLabel.get());
    }

    @Test
    public void getLabelRedirectTest(){
        ULocale usedLanguage = ULocale.forLanguageTag(language);
        String uri = "http://dbpedia.org/resource/AfghanistanMilitary";
        String expectedLabel = "AfghanistanMilitary";
        Optional<String> actualLabel = meaningDictionaryImplementation.getLabel(uri, usedLanguage);
        assertEquals(expectedLabel, actualLabel.get());
    }

    @Test
    public void getGlossesTest(){
        String key = "http://dbpedia.org/resource/Afghan_Armed_Forces";
        ULocale usedLanguage = ULocale.forLanguageTag(language);

        String expectedLabel = "Afghan Armed Forces";
        String expectedAbstract = "The Afghan Armed Forces are the military forces of the Islamic Republic of Afghanistan. They consist of the Afghan National Army and the Afghan Air Force. The President of Afghanistan is the Commander-in-Chief of the Afghan Armed Forces, which is administratively controlled through the Ministry of Defense. The National Military Command Center in Kabul serves as the headquarters of the Afghan Armed Forces. The Afghan Armed Forces currently has approximately 300,000 active duty soldiers and airmen, which are expected to reach 360,000 soldiers and airmen in the coming year.";
        List<String> returnedGlosses = meaningDictionaryImplementation.getGlosses(key, usedLanguage);

        String returnedLabel = returnedGlosses.get(0);
        String returnedAbstract = returnedGlosses.get(1);

        assertEquals(expectedLabel, returnedLabel);
        assertEquals(expectedAbstract, returnedAbstract);
    }

    @Test
    public void getRedirectURITest(){
        ULocale usedLanguage = ULocale.forLanguageTag(language);

        String input = "http://dbpedia.org/resource/AfghanistanMilitary";
        String expected = "http://dbpedia.org/resource/Afghan_Armed_Forces";

        String returned = meaningDictionaryImplementation.getRedirectURI(input, usedLanguage);

        assertEquals(expected, returned);
    }

    @Test
    public void getGlossesRedirectTest(){
        String key = "http://dbpedia.org/resource/AfghanistanMilitary";
        ULocale usedLanguage = ULocale.forLanguageTag(language);

        String expectedLabel = "Afghan Armed Forces";
        String expectedAbstract = "The Afghan Armed Forces are the military forces of the Islamic Republic of Afghanistan. They consist of the Afghan National Army and the Afghan Air Force. The President of Afghanistan is the Commander-in-Chief of the Afghan Armed Forces, which is administratively controlled through the Ministry of Defense. The National Military Command Center in Kabul serves as the headquarters of the Afghan Armed Forces. The Afghan Armed Forces currently has approximately 300,000 active duty soldiers and airmen, which are expected to reach 360,000 soldiers and airmen in the coming year.";
        List<String> returnedGlosses = meaningDictionaryImplementation.getGlosses(key, usedLanguage);

        String returnedLabel = returnedGlosses.get(0);
        String returnedAbstract = returnedGlosses.get(1);

        assertEquals(expectedLabel, returnedLabel);
        assertEquals(expectedAbstract, returnedAbstract);
    }


    @Test
    public void getLexicalizationsWithID(){

        String key = "http://dbpedia.org/resource/History_of_Barcelona";
        String expected1 = "http://dbpedia.org/resource/History_of_barcelona";
        String expected2 = "http://dbpedia.org/resource/Taifa_of_Barcelona";

        Set<String> expectedSet = new HashSet<>();
        expectedSet.add(expected1);
        expectedSet.add(expected2);

        List<Pair<String, POS.Tag>> returned = meaningDictionaryImplementation.getLexicalizations(key);
        String returnedLexicalization1 = returned.get(0).getLeft();
        String returnedLexicalization2 = returned.get(1).getLeft();

        Set<String> returnedSet = new HashSet<>();
        returnedSet.add(returnedLexicalization1);
        returnedSet.add(returnedLexicalization2);

        assertEquals(expectedSet, returnedSet);
    }

    @Test
    public void getLexicalizationsWithLocaleAndID(){

        ULocale usedLanguage = ULocale.forLanguageTag(language);

        String key = "http://dbpedia.org/resource/History_of_Barcelona";
        String expected1 = "http://dbpedia.org/resource/History_of_barcelona";
        String expected2 = "http://dbpedia.org/resource/Taifa_of_Barcelona";

        Set<String> expectedSet = new HashSet<>();
        expectedSet.add(expected1);
        expectedSet.add(expected2);

        List<Pair<String, POS.Tag>> returned = meaningDictionaryImplementation.getLexicalizations(key, usedLanguage);
        String returnedLexicalization1 = returned.get(0).getLeft();
        String returnedLexicalization2 = returned.get(1).getLeft();

        Set<String> returnedSet = new HashSet<>();
        returnedSet.add(returnedLexicalization1);
        returnedSet.add(returnedLexicalization2);

        assertEquals(expectedSet, returnedSet);
    }
}
