package com.mycompany.app;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AppUtilsTest {

    @Test
    public void returnInQuotesTest(){
        String input = "<http://dbpedia.org/resource/Akira_Kurosawa> <http://www.w3.org/2000/01/rdf-schema#label> \"Akira Kurosawa\"@en .";
        String expectedOutput = "Akira Kurosawa";
        String output = AppUtils.returnInQuotes(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void getURIFromLabelsFileTest(){
        String input = "<http://dbpedia.org/resource/Akira_Kurosawa> <http://www.w3.org/2000/01/rdf-schema#label> \"Akira Kurosawa\"@en .";
        String expectedOutput = "http://dbpedia.org/resource/Akira_Kurosawa";
        String output = AppUtils.getURIFromLabelsFile(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void divideRedirectStringTest(){

        String input = "<http://dbpedia.org/resource/AfghanistanMilitary> <http://dbpedia.org/ontology/wikiPageRedirects> <http://dbpedia.org/resource/Afghan_Armed_Forces> . ";
        String expectedURI = "http://dbpedia.org/resource/AfghanistanMilitary";
        String expectedRedirect = "http://dbpedia.org/resource/Afghan_Armed_Forces";
        String[] output = AppUtils.divideRedirectString(input);

        assertEquals(expectedURI, output[0]);
        assertEquals(expectedRedirect, output[1]);

    }

    @Test
    public void returnInBracketsTest(){

        String input = "<http://dbpedia.org/resource/Akira_Kurosawa> <http://www.w3.org/2000/01/rdf-schema#label> \"Akira Kurosawa\"@en .";
        String expectedOutput = "http://dbpedia.org/resource/Akira_Kurosawa";
        String output = AppUtils.returnInBrackets(input);

        assertEquals(output, expectedOutput);

    }
}
