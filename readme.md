This is a collection of indexes used to store information available in DBPedia. 
We use ChronicleMap for the indexes. ChronicleMap function similarly to Redis and other key-value databases. We use ChronicleMap because it is faster for data that can fit in a single server.

Description of the classes:

AppUtils: just utils used to create the index.

IndexBuilder: the class that builds all the indexes. It currently has four indexes (but could easily be expanded). 

	private ChronicleMap<String, String> formToMeaningIndex; // label (title of the article) -> DBPedia article URI
    
    private ChronicleMap<String, String> meaningToFormIndex; // DBPedia article URI -> label
    
    private ChronicleMap<String, String[]> meaningToGlosses; // DBPedia article URI -> glosses (which is the label and a short description of the article). This could be expanded as we include more glosses.

    private ChronicleMap<String, String[]> formToLexicalizations; // label -> redirect URIs (each DBPedia article, just like Wikipedia articles, have articles that exist in name only and automatically redirect to another article. This way, for each article, you can know which ones redirect to it).
	
Note that currently, entries that may have more than one value (such as glosses) as stored as arrays. ChronicleMap allows us to change this into a custom class fairly easily and will done so under request.
When creating the indexes, you have to pass two arguments. First, the path where the files are. The indexes are created based on dbpedia dumps. Second of all, you pass the argument of the language, consisting of two characters in lowercase. For example, english would be "en". This language of choice must be the same as the language of the DBPedia files.
	
Main: this is the main class you may use to create the indexes. The input will be the path to the dump files and the language of choice.

These are the files used to create the index (you have to unzip them):

    https://downloads.dbpedia.org/repo/lts/generic/page/2019.08.30/page_lang=en_ids.ttl.bz2

    https://downloads.dbpedia.org/repo/lts/generic/labels/2019.08.30/labels_lang=en.ttl.bz2

    https://downloads.dbpedia.org/repo/lts/generic/redirects/2019.08.30/redirects_lang=en_transitive.ttl.bz2

    https://downloads.dbpedia.org/repo/lts/mappings/mappingbased-literals/2019.08.30/mappingbased-literals_lang=en.ttl.bz2

    https://databus.dbpedia.org/dbpedia/text/short-abstracts/2020.02.01 Note that this one is different from the one provided in the original document: https://docs.google.com/document/d/1rKhi0F9Oa8t6zENKK83lge-NKDnfFxp3bAGl6LPm3aw This is because the original one is corrupted (this has been confirmed by the developers). 


MeaningDictionary: this is an interface used for compatibility purposes.

MeaningDictionaryImplementation: implementation of MeaningDictionary. Before using other methods, use loadIndexes(). After finishing, use closeIndexes().

POS: class added for compatibility purposes. May be removed if you can add it as an import in a more clean fashion. 
